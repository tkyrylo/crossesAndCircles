package Game;

import java.io.*;
import java.util.*;

final class ScoreDbHelper {

    /**
     * Score list map.
     *
     */
    private static Map<String, Integer> scoreMap = new HashMap<>();

    /**
     * Path to file with players score.
     *
     */
    private static String filePath = "src/Game/scoreMap.txt";

    /**
     * Read players names and their score from scoreMap.txt file in src/ folder
     * and put to the scoreMap map.
     *
     */
    static void readScoreListFile() {
        BufferedReader br = null;
        try {
            String currentLine;
            br = new BufferedReader(new FileReader(filePath));
            if (!filePath.isEmpty()) {
                while ((currentLine = br.readLine()) != null) {
                    String[] pair = currentLine.split("=");
                    scoreMap.put(pair[0], Integer.parseInt(pair[1]));
                }
            }
        }catch (FileNotFoundException ignored){
        }catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Add player to the scoreMap.
     *
     * @param player  player which add to the scoreMap.
     */
    static void addToScoreDb(Player player){
        String playerName = player.getPlayerName();
        if (!scoreMap.containsKey("PC")) scoreMap.put("PC", 0);
        if (!scoreMap.containsKey(playerName)) scoreMap.put(playerName, 0);
    }

    /**
     * Increase score of specified player.
     * Add player name to scoreMap if player name is not in scoreMap
     *
     * @param player player which score will be increased.
     */
    static void increaseScore(Player player) {
        String playerName = player.getPlayerName();
        if (scoreMap.containsKey(playerName)) {
            scoreMap.put(playerName, scoreMap.get(playerName) + 1);
        } else {
            addToScoreDb(player);
            increaseScore(player);
        }
    }


    /**
     * Sort and print scoreMap in readable format.
     *
     */
    static void displayScores() {
        Object[] a = scoreMap.entrySet().toArray();
        Arrays.sort(a, (o1, o2) ->
                ((Map.Entry<String, Integer>) o2).getValue().compareTo(((Map.Entry<String, Integer>) o1).getValue()));
        for (Object e : a) {
            System.out.println(((Map.Entry<String, Integer>) e).getKey() + " : "
                    + ((Map.Entry<String, Integer>) e).getValue());
        }
    }

    /**
     * Print score for specified player in readable format.
     *
     * @param playerName player name which score will be displayed.
     */
    static void displayScoreForSpecifiedPlayer(String playerName){
        if (scoreMap.isEmpty()){
            System.out.println("Score list is empty.\n");
        } else if (scoreMap.containsKey(playerName)){
                    System.out.print(playerName + " score is " + scoreMap.get(playerName) + "\n");
        } else {
            System.out.println("There is no such player in score table.\n");
            System.out.println();
        }
    }

    /**
     * Write players names and their score to scoreMap.txt file in src/ folder.
     *
     */
    static void writeScoreToFile() {
        File file = new File(filePath);
        FileWriter fw;
        BufferedWriter bw = null;
        try {
            fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            for (Map.Entry<String, Integer> entry : scoreMap.entrySet()) {
                bw.write(entry.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally
        {
            try{
                if(bw!=null) bw.close();
            }catch(Exception e){
                System.out.println("Error in closing the BufferedWriter" + e);
            }
        }
    }

}
