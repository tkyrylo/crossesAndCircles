package Game;

final class Settings {

    /**
     * Values for standard and num lock oriented game type.
     */
    static final int STANDARD = 1;
    static final int NUMLOCK = 2;

    /**
     * Values for field display types.
     */
    static final int UNDERSCORE = 1;
    static final int DIGITS = 2;

    /**
     * Values for game difficulty.
     */
    static final int EASY = 1;
    static final int NORMAL = 2;

    /**
     * Value for field type.
     */
    private static int fieldType = STANDARD;

    /**
     * Value for field type displaying.
     */
    private static int fieldDisplayType = UNDERSCORE;

    /**
     * Value for game difficulty mode.
     */
    private static int gameMode = EASY;

    /**
     * Set field type value.
     *
     * @param fieldTypeSelect    user select for field type.
     *
     */
    static void setFieldType(int fieldTypeSelect) {
    if (fieldTypeSelect == STANDARD || fieldTypeSelect == NUMLOCK) {
            fieldType = fieldTypeSelect;
        } else {
            Menu.displayIncorrectChoice();
        }
    }

    /**
     * Getter for field type.
     *
     */
    static int getFieldType(){
        return fieldType;
    }

    /**
     * Set field type display value.
     *
     * @param fieldDisplayTypeSelect    user select for field display type.
     *
     */
    static void setFieldDisplayType(int fieldDisplayTypeSelect) {
        if (fieldDisplayTypeSelect == UNDERSCORE || fieldDisplayTypeSelect == DIGITS) {
            fieldDisplayType = fieldDisplayTypeSelect;
        } else {
            Menu.displayIncorrectChoice();
        }
    }

    /**
     * Getter for field type display.
     *
     */
    static int getFieldDisplayType(){
        return fieldDisplayType;
    }

    /**
     * Set game mode value.
     *
     * @param gameModeSelect    user select for game mode.
     *
     */
    static void setGameMode(int gameModeSelect){
        if (gameModeSelect == EASY || gameModeSelect == NORMAL) {
            gameMode = gameModeSelect;
        } else {
            Menu.displayIncorrectChoice();
        }
    }

    /**
     * Getter for game mode.
     *
     */
    static int getGameMode() {
        return gameMode;
    }
}
