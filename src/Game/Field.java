package Game;


import Game.Exceptions.FieldBusyException;
import Game.Exceptions.IncorrectValueException;

class Field {

    /**
     * Game field as an array.
     */
    private int[] gameField = new int[9];

    /**
     * Game field for numlock type display.
     */
    private int[] numlockField = {7, 8, 9, 4, 5, 6, 1, 2, 3};

    /**
     * Getter for fameField array.
     */
    int[] getGameField(){
        return gameField;
    }

    /**
     * Print gameField depend on fieldDisplayType settings in readable format.
     */
    void displayGameField() {
        for (int i=0; i<gameField.length;) {
            switch (gameField[i]) {
                case Main.CROSS:
                    System.out.print("X ");
                    i++;
                    break;
                case Main.CIRCLE:
                    System.out.print("O ");
                    i++;
                    break;
                default:
                    if (Settings.getFieldDisplayType() == Settings.UNDERSCORE) {
                        System.out.print("_ ");
                    } else if (Settings.getFieldType() == Settings.STANDARD &&
                            Settings.getFieldDisplayType() == Settings.DIGITS){
                        System.out.print(i + " ");
                    } else if (Settings.getFieldType() == Settings.NUMLOCK &&
                            Settings.getFieldDisplayType() == Settings.DIGITS) {
                        System.out.print(numlockField[i] + " ");
                    }
                    i++;
                    break;
            }
            if (i % 3 == 0) System.out.println();
        }
    }

    /**
     * Check is selected value is valid, change value if Settings fieldDisplayType setup as NUMLOCK oriented and
     * if selected cell is empty and put playerMark in it.
     *
     * @param cellNumber    cell number which will be set.
     * @param playerMark    player mark which will be set.
     *
     * @return true if winner found.
     */
    boolean setCell(int cellNumber, int playerMark) throws FieldBusyException, IncorrectValueException{
        if (cellNumber >= 1 && cellNumber <= 9){
            if (Settings.getFieldType() == Settings.NUMLOCK){
                if (cellNumber == 1) cellNumber = 7;
                else if (cellNumber == 2) cellNumber = 8;
                else if (cellNumber == 3) cellNumber = 9;
                else if (cellNumber == 7) cellNumber = 1;
                else if (cellNumber == 8) cellNumber = 2;
                else if (cellNumber == 9) cellNumber = 3;
            }
            if (gameField[cellNumber - 1] == Main.EMPTY) {
                gameField[cellNumber - 1] = playerMark;
                return true;
            } else{
                throw new FieldBusyException();
            }
        } else{
            throw new IncorrectValueException();
        }
    }

    /**
     * Check winner in game and print winner name if winner found.
     * Lose combination available only in solo game with PC.
     *
     * Use {@link #checkWinCombination(int player)} to check win combination at game field.
     * Use {@link #isFieldFull()} to stop the game if game field is full.
     *
     * @param player    player instance which will be checked as a possible winner.
     *
     * @return true if winner found.
     */
    boolean checkWinner(Player player) {
        String playerName = player.getPlayerName();
        int playerMark = player.getPlayerMark();
        if (checkWinCombination(playerMark)) {
            Menu.displayWiner(playerName);
            return true;
        }
        if (isFieldFull()){
            Menu.displayFrendshipWins();
            return true;
        }
        return false;
    }

    /**
     * Check eight win combination in game.
     *
     * @param mark  mark which will be checked in win combination.
     * @return      true if win combination is found.
     */
    private boolean checkWinCombination(int mark) {
        return gameField[0] == mark && gameField[1] == mark && gameField[2] == mark ||
                gameField[3] == mark && gameField[4] == mark && gameField[5] == mark ||
                gameField[6] == mark && gameField[7] == mark && gameField[8] == mark ||
                gameField[0] == mark && gameField[3] == mark && gameField[6] == mark ||
                gameField[1] == mark && gameField[4] == mark && gameField[7] == mark ||
                gameField[2] == mark && gameField[5] == mark && gameField[8] == mark ||
                gameField[0] == mark && gameField[4] == mark && gameField[8] == mark ||
                gameField[2] == mark && gameField[4] == mark && gameField[6] == mark;
    }

    /**
     * Check fullness of game field.
     *
     * @return true if game field is full.
     */
    private boolean isFieldFull() {
        for (int cell : gameField) {
            if (cell == 0) return false;
        }
        return true;
    }
}
