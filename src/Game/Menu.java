package Game;


final class Menu {

    static private String mainMenu = "Main menu\n" +
            "Please make your choice:\n" +
            "[1] New game\n" +
            "[2] Rules\n" +
            "[3] Show score\n" +
            "[4] Settings\n" +
            "[5] Exit";

    static private String gameMenu = "Please select game type:\n\n" +
            "[1] Player vs PC\n" +
            "[2] Player vs Player\n" +
            "[3] Back to menu";

    static private String selectMark = "Please select what mark you want to play\n" +
            "[1] Play for crosses\n" +
            "[2] Play for circles";

    static private String scoreMenu = "\nPlease select:\n\n" +
            "[1] All players score\n" +
            "[2] Score for specified player\n" +
            "[3] Back to menu";

    static private String settingsMenu = "\nPlease select:\n\n" +
            "[1] Set game field type\n" +
            "[2] Set field display type\n" +
            "[3] Set game mode\n" +
            "[4] Back to menu";

    static private String selectFieldDisplayNormal = "Please select how display field:\n" +
            "[1] Standard field\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "[2] Field with digits\n" +
            "7 8 9\n" +
            "4 5 6\n" +
            "1 2 3\n" +
            "[3] Back";

    static private String selectFieldDisplayNumLock = "Please select how display field:\n" +
            "[1] Standard field\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "[2] Field with digits\n" +
            "1 2 3\n" +
            "4 5 6\n" +
            "7 8 9\n" +
            "[3] Back";

    static private String selectFieldType = "Please select field type:\n" +
            "[1] Standard game field\n" +
            "1 2 3\n" +
            "4 5 6\n" +
            "7 8 9\n" +
            "[2] Num lock oriented field\n" +
            "7 8 9\n" +
            "4 5 6\n" +
            "1 2 3\n" +
            "[3] Back";

    static private String selectGameDifficultyy = "Please select game mode:\n" +
            "[1] Easy\n" +
            "[2] Normal\n" +
            "[3] Back";

    static private String settingsSet = "Settings saved!\n";

    static private String selectGameMode = "Please select game mode:";

    static private String specifiedPlayerEnterName = "Please enter player name:";

    static private String incorrectChoice = "Incorrect choice!\n";

    static private String enterDigits = "Please enter digits.";

    static private String soloGameEnterName = "Please enter Your name:";

    static private String firstPlayerEnterName = "Please enter first Player name:";

    static private String secondPlayerEnterName = "Please enter second Player name:";

    static private String rules = "" +
            "*************************\n" +
            "Rules of game Tic-tac-toe:\n" +
            "The player who succeeds in placing three of their marks in a" +
            " horizontal, vertical, or diagonal row wins the game.\n\n" +
            "New game filed is displayed as:\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "_ _ _\n" +
            "Where cells numbers:\n" +
            "1 2 3\n" +
            "4 5 6\n" +
            "7 8 9\n" +
            "Player should enter cell number to fill empty cell by his mark.\n" +
            "Game types: Player vs PC and Player vs Player\n" +
            "Also Player has possibility to select mark type.\n\n" +
            "*************************\n";

    static private String end = "\nBye!\n*************************\n";

    static private String friendshipWins = "Friendship wins!!!! ;) \n";

    static private String enterCell = "Enter cell number:";

    static private String cellIsBusy = "Current cell is busy! Please try another one.\n";

    static void displayMainMenu(){
        System.out.println(mainMenu);
    }

    static void displayGameMenu(){
        System.out.println(gameMenu);
    }

    static void displayScoreMenu(){
        System.out.println(scoreMenu);
    }

    static void displaySettingsMenu() {
        System.out.println(settingsMenu);
    }

    static void displaySelectGameDificulty() {
        System.out.println(selectGameDifficultyy);
    }

    static void displaySpecifiedPlayerEnterName(){
        System.out.println(specifiedPlayerEnterName);
    }

    static void displayTurn(String playerName){
        System.out.println(playerName + " turn:");
    }

    static void displayEnterCell(){
        System.out.println(enterCell);
    }

    static void displayIncorrectChoice(){
        System.out.println(incorrectChoice);
    }

    static void displayEnterDigits(){
        System.out.println(enterDigits);
    }

    static void displaySelectFieldType(){
        System.out.println(selectFieldType);
    }

    static void displaySelectFieldDisplay(){
        if (Settings.getFieldDisplayType() == Settings.UNDERSCORE) {
            System.out.println(selectFieldDisplayNormal);
        } else {
            System.out.println(selectFieldDisplayNumLock);
        }
    }

    static void displaySettingsSaved(){
        System.out.println(settingsSet);
    }

    static void displaySelectGameMode(){
        System.out.println(selectGameMode);
    }

    static void displayCellIsBusy(){
        System.out.println(cellIsBusy);
    }

    static void displaySoloGameEnterName(){
        System.out.println(soloGameEnterName);
    }

    static void displaySelectMark(){
        System.out.println(selectMark);
    }

    static void displayFirstPlayerEnterName(){
        System.out.println(firstPlayerEnterName);
    }

    static void displaySecondPlayerEnterName(){
        System.out.println(secondPlayerEnterName);
    }

    static void displayRules(){
        System.out.println(rules);
    }

    static void displayWiner(String playerName){
        System.out.println(playerName + " wins!");
    }

    static void displayFrendshipWins(){
        System.out.println(friendshipWins);
    }

    static void displayExit(){
        System.out.println(end);
    }

}
