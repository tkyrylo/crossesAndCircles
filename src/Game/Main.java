package Game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Main {

    /**
     * Values for Cross, Circle and Empty cell
     */
    static final int EMPTY = 0;
    static final int CROSS = 1;
    static final int CIRCLE = 2;

    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.runMainMenu();
    }

    /**
     * Run main menu.
     * <p>
     * Main menu contain next selections:
     * New game which open Sub menu (Game menu)
     * Rules which display rules
     * Show score which open Score menu
     * Settings which open Settings menu
     * Exit which stop program
     * User can select items by entering digits in range from 1 to 4.
     *
     */
    private void runMainMenu(){
        ScoreDbHelper.readScoreListFile(); // Read scoreMap file from Game package folder.
        int mainMenuChoice = 0; // Main menu selector
        do { // Main menu cycle
            try {
                Menu.displayMainMenu(); // Print main menu
                mainMenuChoice = getIntegerFromConsole();
                switch (mainMenuChoice) {
                    case 1:
                        runGameMenu(); // Run submenu method
                        break;
                    case 2:
                        Menu.displayRules(); // Display game rules
                        break;
                    case 3:
                        runScoreMenu(); // Run score menu
                        break;
                    case 4:
                        runSettingsMenu(); // Run settings menu
                        break;
                    case 5:
                        ScoreDbHelper.writeScoreToFile(); // Save scoreMap in Game package folder.
                        Menu.displayExit(); // Exit
                        break;
                    default:
                        Menu.displayIncorrectChoice();
                        break;
                }
            } catch (NumberFormatException e) {
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (mainMenuChoice != 5);
    }

    /**
     * Run Game menu for game type select.
     * <p>
     * Game menu contain next selections:
     * Player vs PC game which starts solo game with PC
     * Player vs Player game which starts two players game
     * Back to menu which back to main menu
     * User can select items by entering digits in range from 1 to 3.
     *
     */
    private void runGameMenu() throws IOException {
        int gameMenuChoice = 0; // SubMenu selector
        do {
            Menu.displayGameMenu(); //Display game menu
            try {
                gameMenuChoice = getIntegerFromConsole();
            } catch (NumberFormatException e){
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (gameMenuChoice) {
                case 1: // Solo game with PC
                    onePlayerGame();
                    break;
                case 2: // Game for two players
                    twoPlayersGame();
                    break;
                case 3:
                    gameMenuChoice = 3;
                    break;
                default:
                    Menu.displayIncorrectChoice();
                    break;
            }
        } while (gameMenuChoice != 3);
    }

    /**
     * Run Score menu for score display selection.
     * <p>
     * Score Menu contain next selections:
     * All players score which show score for all players
     * Score for specified player which show score for specified player
     * Back to menu which back to main menu
     * User can select items by entering digits in range from 1 to 3.
     *
     */
    private void runScoreMenu()throws IOException{
        int scoreMenuChoice = 0; // ScoreMenu selector
        do {
            Menu.displayScoreMenu(); //Display score menu
            try {
                scoreMenuChoice = getIntegerFromConsole();
            } catch (NumberFormatException e){
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (scoreMenuChoice) {
                case 1:
                    ScoreDbHelper.displayScores(); // Display score for all players
                    break;
                case 2:
                    String playerName = null;
                    Menu.displaySpecifiedPlayerEnterName();
                    try {
                        playerName = getStringFromConsole();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ScoreDbHelper.displayScoreForSpecifiedPlayer(playerName); // Display score for specified player
                    break;
                case 3:
                    scoreMenuChoice = 3;
                    break;
                default:
                    Menu.displayIncorrectChoice();
                    break;
            }
        } while (scoreMenuChoice != 3);
    }

    /**
     * Run Settings menu for settings setup.
     *
     */
    private void runSettingsMenu()throws IOException{
        int scoreSettingsMenu = 0; // SettingsMenu selector
        do {
            Menu.displaySettingsMenu(); //Display settings menu
            try {
                scoreSettingsMenu = getIntegerFromConsole();
            } catch (NumberFormatException e){
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (scoreSettingsMenu) {
                case 1:
                    fieldTypeMenu();
                    break;
                case 2:
                    fieldDisplayTypeMenu();
                    break;
                case 3:
                    gameModeMenu();
                    break;
                case 4:
                    scoreSettingsMenu = 4;
                    break;
                default:
                    Menu.displayIncorrectChoice();
                    break;
            }
        } while (scoreSettingsMenu != 4);
    }

    /**
     * Run field type menu for field orientation setup.
     *
     */
    private void fieldTypeMenu(){
        int fieldTypeSelect = 0;
        do {
            Menu.displaySelectFieldType();
            try {
                fieldTypeSelect = getIntegerFromConsole();
            } catch (NumberFormatException e) {
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (fieldTypeSelect) {
                case 1:
                    Settings.setFieldType(Settings.STANDARD);
                    Menu.displaySettingsSaved();
                    fieldTypeSelect = 3;
                    break;
                case 2:
                    Settings.setFieldType(Settings.NUMLOCK);
                    Menu.displaySettingsSaved();
                    fieldTypeSelect = 3;
                    break;
                case 3:
                    fieldTypeSelect = 3;
                    break;
            }
        }while (fieldTypeSelect != 3);
    }

    /**
     * Run field display menu for field display setup.
     *
     */
    private void fieldDisplayTypeMenu(){
        int fieldDisplayTypeSelect = 0;
        do {
            Menu.displaySelectFieldDisplay();
            try {
                fieldDisplayTypeSelect = getIntegerFromConsole();
            } catch (NumberFormatException e) {
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (fieldDisplayTypeSelect) {
                case 1:
                    Settings.setFieldDisplayType(Settings.UNDERSCORE);
                    Menu.displaySettingsSaved();
                    fieldDisplayTypeSelect = 3;
                    break;
                case 2:
                    Settings.setFieldDisplayType(Settings.DIGITS);
                    Menu.displaySettingsSaved();
                    fieldDisplayTypeSelect = 3;
                    break;
                case 3:
                    fieldDisplayTypeSelect = 3;
                    break;
            }
        }while (fieldDisplayTypeSelect != 3);
    }

    /**
     * Run game mode menu for game difficulty setup.
     *
     */
    private void gameModeMenu(){
        int gameModeMenuSelect = 0;
        do {
            Menu.displaySelectGameDificulty();
            try {
                gameModeMenuSelect = getIntegerFromConsole();
            } catch (NumberFormatException e) {
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (gameModeMenuSelect) {
                case 1:
                    Settings.setGameMode(Settings.EASY);
                    Menu.displaySettingsSaved();
                    gameModeMenuSelect = 3;
                    break;
                case 2:
                    Settings.setGameMode(Settings.NORMAL);
                    Menu.displaySettingsSaved();
                    gameModeMenuSelect = 3;
                    break;
                case 3:
                    gameModeMenuSelect = 3;
                    break;
            }
        }while (gameModeMenuSelect != 3);
    }

    /**
     * Run game between player and PC.
     *
     */
    private void onePlayerGame() throws IOException{

        String soloPlayerName;
        int playerMark;
        Player soloPlayer;
        PC pc;
        boolean isGameRun = true;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Field gameField = new Field();
        Menu.displaySoloGameEnterName();
        soloPlayerName = getStringFromConsole();
        Menu.displaySelectMark();
        while (true) {
            try {
                playerMark = Integer.parseInt(br.readLine());
                if (playerMark == CROSS || playerMark == CIRCLE) break;
            }catch (NumberFormatException e){
                Menu.displayEnterDigits();
            }catch (IOException e) {
                e.printStackTrace();
            }
            Menu.displayIncorrectChoice();
            Menu.displaySelectMark();
        }

        soloPlayer = new Player(soloPlayerName, playerMark); // Create new player
        pc = new PC(soloPlayer.getPlayerMark()); // Create pc player
        ScoreDbHelper.addToScoreDb(soloPlayer); // Add player and PC to scoreList
        gameField.displayGameField();

        while(isGameRun) {
            isGameRun = runGame(gameField, soloPlayer, pc);
        }
    }

    /**
     * Run game between player and player.
     *
     */
    private void twoPlayersGame() throws IOException{

        String firstPlayerName, secondPlayerName;
        int firstPlayerMark, secondPlayerMark;
        Player firstPlayer, secondPlayer;
        boolean isGameRun = true;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Field gameField = new Field();
        Menu.displayFirstPlayerEnterName();
        firstPlayerName = br.readLine(); // Get first player name
        Menu.displaySelectMark();
        while (true) { // Cycle for first player mark selection and set opposite mark to second player
            try {
                firstPlayerMark = Integer.parseInt(br.readLine());
                if (firstPlayerMark == CROSS) {
                    secondPlayerMark = CIRCLE;
                    break;
                } else if (firstPlayerMark == CIRCLE) {
                    secondPlayerMark = CROSS;
                    break;
                }
            }catch (NumberFormatException e){
                Menu.displayEnterDigits();
            }catch (IOException e) {
                e.printStackTrace();
                Menu.displaySelectMark();
            }
        }
        Menu.displaySecondPlayerEnterName();
        secondPlayerName = br.readLine(); // Get second player name
        firstPlayer = new Player(firstPlayerName, firstPlayerMark); // Create first player
        secondPlayer = new Player(secondPlayerName, secondPlayerMark); // Create second player
        ScoreDbHelper.addToScoreDb(firstPlayer); // Add first player to scoreList
        ScoreDbHelper.addToScoreDb(secondPlayer); // Add second player to scoreList
        gameField.displayGameField();

        while(isGameRun) { // Two players game cycle
            isGameRun = runGame(gameField, firstPlayer, secondPlayer);
        }
    }

    /**
     * Run game between player and player.
     *
     * @param gameField Field gameField instance which field in game process.
     * @param firstPlayer Player instance which play in game.
     * @param secondPlayer Player or PC instance which play in game
     *
     */
    private boolean runGame(Field gameField, Player firstPlayer, Player secondPlayer){
        firstPlayer.setValue(gameField); // Player add mark to field
        gameField.displayGameField();
        if (gameField.checkWinner(firstPlayer)) { // Check for winner
            ScoreDbHelper.increaseScore(firstPlayer); // Increase winner score
            return false;
        }
        secondPlayer.setValue(gameField); // second Player or PC add mark to field
        gameField.displayGameField();
        if (gameField.checkWinner(secondPlayer)) { // Check for winner
            ScoreDbHelper.increaseScore(secondPlayer); // Increase PC score
            return false;
        }
        return true;
    }

    /**
     * Get integer from console.
     *
     */
    private int getIntegerFromConsole() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return Integer.parseInt(br.readLine());
    }

    /**
     * Get String from console.
     *
     */
    private String getStringFromConsole() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        return br.readLine();
    }
}