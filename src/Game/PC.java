package Game;

import Game.Exceptions.FieldBusyException;
import java.util.Random;

class PC extends Player{

    /**
     * PC mark which is opposite to player mark.
     */
    private int pcMark;

    /**
     * Player mark.
     */
    private int playerMark;

    /**
     * PC name.
     */
    private String pcName = "PC";

    @Override
    String getPlayerName(){
        return pcName;
    }

    @Override
    int getPlayerMark(){
        return pcMark;
    }

    /**
     * PC constructor which set PC mark as opposite value to player mark.
     *
     * @param playerMark    player mark which helps to set pcMark.
     */
    PC(int playerMark){
        super("PC", playerMark);
        this.playerMark = playerMark;
        if (playerMark == 1) this.pcMark = Main.CIRCLE;
        else this.pcMark = Main.CROSS;
    }

    /**
     * Set cell number depend on game mode and run gameField.setCell method with this value.
     *
     * @param gameField game field with current marks.
     *
     */
    @Override
    void setValue(Field gameField){
        int cellNumber = new Random().nextInt(9) + 1;
        Menu.displayTurn(pcName);
        boolean isValueSetInCell = false;
        while (!isValueSetInCell) {
            if (Settings.getGameMode() == Settings.NORMAL) {
                cellNumber = normalModeSetValue(gameField.getGameField());
            }
            try {
                System.out.println(cellNumber);
                isValueSetInCell = gameField.setCell(cellNumber, pcMark);
            }catch (FieldBusyException ignored){
            }
        }
    }

    /**
     * Set cell number depend on filed fullness.
     *
     * @param gameField game field with current marks.
     */
    private int normalModeSetValue(int[] gameField){
        if ((gameField[0] == playerMark && gameField[1] == playerMark ||
                gameField[4] == playerMark && gameField[6] == playerMark ||
                gameField[5] == playerMark && gameField[8] == playerMark) && gameField[2] == Main.EMPTY){
            return 3;
        }else if ((gameField[1] == playerMark && gameField[2] == playerMark ||
                gameField[4] == playerMark && gameField[8] == playerMark ||
                gameField[3] == playerMark && gameField[6] == playerMark) && gameField[0] == Main.EMPTY){
            return 1;
        }else if ((gameField[0] == playerMark && gameField[3] == playerMark ||
                gameField[2] == playerMark && gameField[4] == playerMark ||
                gameField[7] == playerMark && gameField[8] == playerMark) && gameField[6] == Main.EMPTY){
            return 7;
        }else if ((gameField[2] == playerMark && gameField[5] == playerMark ||
                gameField[0] == playerMark && gameField[4] == playerMark ||
                gameField[6] == playerMark && gameField[7] == playerMark) && gameField[8] == Main.EMPTY){
            return 9;
        }else if (gameField[1] == playerMark && gameField[4] == playerMark && gameField[7] == Main.EMPTY){
            return 8;
        }else if (gameField[4] == playerMark && gameField[7] == playerMark && gameField[1] == Main.EMPTY){
            return 2;
        }else if (gameField[3] == playerMark && gameField[4] == playerMark && gameField[5] == Main.EMPTY){
            return 6;
        }else if (gameField[4] == playerMark && gameField[5] == playerMark && gameField[3] == Main.EMPTY){
            return 4;
        }else return new Random().nextInt(9) + 1;
    }

}
