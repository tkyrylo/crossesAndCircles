package Game;

import Game.Exceptions.FieldBusyException;
import Game.Exceptions.IncorrectValueException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Player {

    /**
     * Player name.
     */
    private String playerName;

    /**
     * Player mark 1 (cross) or 2 (circle).
     */
    private int playerMark;

    /**
     * Getter for playerName.
     */
    String getPlayerName(){
        return playerName;
    }

    /**
     * Getter for playerMark.
     */
    int getPlayerMark(){
        return playerMark;
    }

    /**
     * Player constructor which set player name, mark and set PC mark opposite to player.
     *
     * @param playerName player name which add to the scoreList.
     * @param playerMark player mark which helps to set pcMark.
     */
    Player(String playerName, int playerMark){
        this.playerName = playerName;
        this.playerMark = playerMark;
    }

    /**
     * Read value from console input and run gameField.setCell method with this value.
     *
     * @param gameField game field with current marks.
     */
    void setValue(Field gameField){
        boolean isValueSetInCell = false;
        Menu.displayTurn(playerName);
        Menu.displayEnterCell();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        while (!isValueSetInCell) {
            try {
                int cellNumber = Integer.parseInt(br.readLine());
                isValueSetInCell = gameField.setCell(cellNumber, playerMark);
            } catch (FieldBusyException e) {
                Menu.displayCellIsBusy();
            } catch (IncorrectValueException e){
                Menu.displayIncorrectChoice();
            } catch (NumberFormatException e) {
                Menu.displayEnterDigits();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
